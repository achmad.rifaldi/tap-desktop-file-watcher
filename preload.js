const { contextBridge, ipcRenderer } = require("electron");

contextBridge.exposeInMainWorld("electron", {
  openDialog: (method, config) => ipcRenderer.invoke("dialog", method, config),
  startScan: (pwd) => ipcRenderer.invoke("startScan", pwd),
  stopScan: () => ipcRenderer.invoke("stopScan"),
  readConfig: () => ipcRenderer.invoke("readConfig"),
  writeConfig: (data) => ipcRenderer.invoke("writeConfig", data),
  onStart: (channel, func) => {
    let validChannels = ["onStart"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  onFileAdded: (channel, func) => {
    let validChannels = ["onFileAdded"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  onFileError: (channel, func) => {
    let validChannels = ["onFileError"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  onUploading: (channel, func) => {
    let validChannels = ["onUploading"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  onPathSelected: (channel, func) => {
    let validChannels = ["onPathSelected"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  onWatched: (channel, func) => {
    let validChannels = ["onWatched"];
    if (validChannels.includes(channel)) {
      // Deliberately strip event as it includes `sender`
      ipcRenderer.on(channel, (event, ...args) => func(...args));
    }
  },
  nodeVersion: process.versions.node,
  chromeVersion: process.versions.chrome,
  electronVersion: process.versions.electron,
});
