const { app, BrowserWindow, ipcMain, dialog, Tray } = require("electron");
const path = require("path");
const chokidar = require("chokidar");
const express = require("express");
const cors = require("cors");
const compression = require("compression");
const bodyParser = require("body-parser");
const axios = require("axios");
const retry = require('retry');
const FormData = require("form-data");
const fs = require("fs");
const serverExpress = express();

let watcher;
let files = [];
let errorFiles = [];
let watchedFiles = [];
let watchReady = false;
let win = null;

let isWin = process.platform === "win32";

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
    },
    icon: path.join(__dirname, "./resources/assets/icon.ico"),
    autoHideMenuBar: true,
    alwaysOnTop: true,
  });

  // Load the index.html of the app.
  win.loadFile("src/index.html");

  // Open the DevTools.
  // win.webContents.openDevTools();
}

app.setLoginItemSettings({
  openAtLogin: true,
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// This method is equivalent to 'app.on('ready', function())'
app.whenReady().then(() => {
  createWindow();

  /**
   *
   * Express Server
   *
   */

  serverExpress.use(function setCommonHeaders(req, res, next) {
    res.set("Access-Control-Allow-Private-Network", "true");
    next();
  });

  serverExpress.use(cors({ withCredentials: true }));

  // parse application/x-www-form-urlencoded
  serverExpress.use(bodyParser.urlencoded({ extended: false }));

  // parse application/json
  serverExpress.use(bodyParser.json());

  serverExpress.set("port", process.env.PORT || 2022);

  serverExpress.use(compression());

  serverExpress.post("/start-scan", async (req, res) => {
    const dialogConfig = {
      title: "Select a folder",
      buttonLabel: "Choose",
      properties: ["openDirectory"],
    };

    win.webContents.send("onStart", true);

    res.json({ message: "OK" });
  });

  serverExpress.post("/files", async (req, res) => {
    res.json({ message: "OK", data: files });
  });

  serverExpress.get("/current-status", async (req, res) => {
    res.json({
      message: "OK",
      data: {
        status: watchReady,
      },
    });
  });

  serverExpress.post("/stop-scan", async (req, res) => {
    stopWatcher();
    res.json({ message: "OK", data: {
      files,
      errorFiles
    } });
  });

  serverExpress.listen(serverExpress.get("port"), "0.0.0.0", function () {
    console.log("service running on port", serverExpress.get("port"));
  });

  /**
   *
   * Event Emmiter
   *
   */

  ipcMain.handle("dialog", (event, method, params) => {
    return dialog[method](win, params).then((result) => result);
  });

  ipcMain.handle("startScan", (event, pwd) => {
    startWatcher(pwd);
  });
  ipcMain.handle("stopScan", (event) => {
    stopWatcher();
  });
  ipcMain.handle("readConfig", (event) => {
    let dataConfig = null;
    try {
      dataConfig = fs.readFileSync("path.txt", 'utf8');
    } catch (err) {
      fs.openSync("path.txt", 'w');
    }
    return dataConfig;
  });
  ipcMain.handle("writeConfig", (event, data) => {
    fs.writeFileSync("path.txt", data);
  });
});

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // To stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the
  // app when the dock icon is clicked and there are no
  // other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

const startWatcher = (folderPath) => {
  watcher = chokidar.watch(folderPath, {
    ignored: /[\/\\]\./,
    persistent: true,
  });

  watcher
    .on("error", function (error) {
      console.log("Error happened", error);
    })
    .on("ready", function () {
      watchReady = true;
      files = [];
      watchedFiles = [];
      errorFiles = [];
      win.webContents.send("onPathSelected", folderPath);
      win.webContents.send("onWatched", true);
    })
    .on("raw", async function (event, pwd, details) {
      setTimeout(function(){      
        /**
         * win event -> change
         */
        if (event == "created" || event == "change") {
          /**
           * make sure file upload not duplicate
           * on win theres a chance that event "change" trigered multiple times!
           */
          if (watchReady && !watchedFiles.includes(pwd)) {
            if (path.extname(pwd) === '.pdf' || path.extname(pwd) === '.jpg' || path.extname(pwd) === '.jpeg' || path.extname(pwd) === '.png') {
                watchedFiles.push(pwd);
                uploadFile(folderPath, pwd);
            } else {

              /**
               * make sure log not duplicate
               * on win theres a chance that event "change" trigered multiple times!
               */
              if (!errorFiles.includes(pwd)) {
                errorFiles.push(pwd);
                win.webContents.send("onFileError", `${pwd} not in pdf format.`);
              }
            }
          }
        }
      }, 7000);  
      // This event should be triggered everytime something happens.
      console.log("Raw event info:", event, pwd, details);
    });
};

const stopWatcher = () => {
  watcher.close().then(() => {
    watchReady = false;
    win.webContents.send("onWatched", false);
  });
};

const uploadFile = (folderPath, pwd) => {
  const operation = retry.operation({
    retries: 3,
    factor: 2,
    minTimeout: 1 * 1000,
    maxTimeout: 30 * 1000,
    randomize: false,
  });

  operation.attempt((currentAttempt) => {
    win.webContents.send("onUploading", `(${currentAttempt}) ${pwd}`);

    const bodyFormData = new FormData();
    const filePath = isWin ? fs.createReadStream(path.join(folderPath, pwd)) : pwd
    bodyFormData.append(
      "file",
      filePath
    );

    axios({
      method: "post",
      url: "http://vmsdev.tap-agri.com/store/media",
      data: bodyFormData,
      headers: { "Content-Type": "multipart/form-data" },
    })
      .then(function (response) {
        files.push(response.data);
        win.webContents.send("onFileAdded", pwd);
        fs.unlinkSync(isWin ? filePath.path : filePath);
      })
      .catch(function (error) {
        if (operation.retry(error)) { return; }

        console.log(error)
        win.webContents.send("onFileError", `${pwd} - ${error.message}`);
      });
  });
};
