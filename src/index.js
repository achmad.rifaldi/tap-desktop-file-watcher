// Importing dialog module using remote
const dialog = electron.openDialog;

const startButton = document.getElementById("start");
const stopButton = document.getElementById("stop");
const activePath = document.getElementById("active-path");
let showInLogFlag = false;

startButton.addEventListener("click", async () => {
  const config = await electron.readConfig();
  if (!config || config === '') {
    const dialogConfig = {
      title: "Select a folder",
      buttonLabel: "Choose",
      properties: ["openDirectory"],
    };

    electron.openDialog("showOpenDialog", dialogConfig).then((result) => {
      electron.writeConfig(result.filePaths[0]);
      electron.startScan(result.filePaths[0]);
    });
  } else {
    console.log(config);
    electron.startScan(config);
  }
});

stopButton.addEventListener("click", () => {
  electron.stopScan();
});

electron.onWatched("onWatched", (data) => {
  if (data) {
    startButton.setAttribute("disabled", " ");
    stopButton.removeAttribute("disabled");

    showInLogFlag = true;
  } else {
    startButton.removeAttribute("disabled");
    stopButton.setAttribute("disabled", " ");
    resetLog();
  }
});

electron.onPathSelected("onPathSelected", (data) => {
  console.log(data)
  if (data) {
    activePath.innerHTML = data;
  } else {
    activePath.innerHTML = '';
  }
});

electron.onStart("onStart", (data) => {
  startButton.click();
});

electron.onFileAdded("onFileAdded", (data) => {
  if (showInLogFlag) {
    addLog(`[SUCCESS] ${data}`, "add");
  }
});

electron.onFileError("onFileError", (data) => {
  if (showInLogFlag) {
    addLog(`[ERROR] ${data}`, "delete");
  }
});

electron.onUploading("onUploading", (data) => {
  if (showInLogFlag) {
    addLog(`[UPLOADING] ${data}`, "change");
  }
});

function resetLog() {
  return (document.getElementById("log-container").innerHTML = "");
}

function addLog(message, type) {
  let el = document.getElementById("log-container");
  let newItem = document.createElement("LI"); // Create a <li> node
  let textnode = document.createTextNode(message); // Create a text node
  if (type == "delete") {
    newItem.style.color = "red";
  } else if (type == "change") {
    newItem.style.color = "blue";
  } else {
    newItem.style.color = "green";
  }

  newItem.appendChild(textnode); // Append the text to <li>
  el.appendChild(newItem);
}
